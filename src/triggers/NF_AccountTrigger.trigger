/**
 * Created by vravikumar on 9/19/2019.
 */

trigger NF_AccountTrigger on Account (
        before insert,
        before update,
        before delete,
        after insert,
        after update,
        after delete,
        after undelete ) {
    NF_TriggerFactory.CreateHandlerAndExecute(Account.sObjectType);
}