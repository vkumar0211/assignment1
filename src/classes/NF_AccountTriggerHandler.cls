
/**
* @File Name:   NF_AccountTriggerHandler.cls
* @Description:
* @Author:      Recruiter
* @Group:       Apex
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2017-05-09  Recruiter    Created the file/class
*/
    public with sharing class NF_AccountTriggerHandler extends NF_AbstractTriggerHandler {
        public override void beforeUpdate(){

        }

        public override void afterUpdate(){

        }

        public override void beforeInsert(){

        }

        public override void afterInsert(){
            CreateContactFromAccount.createContact(Trigger.new);
        }

        public override void afterDelete(){

        }

        public override void andFinally(){

        }
    }