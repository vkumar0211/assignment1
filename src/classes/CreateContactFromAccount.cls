public with sharing class CreateContactFromAccount {
 public static void createContact(list<Account> accounts){
     list<Contact> contacts_ls=new list<Contact>();
     for(Account acc : accounts){
         Contact con = new Contact(LastName = acc.Name, AccountId = acc.Id);
         contacts_ls.add(con);
     }
     insert contacts_ls;
 }
}