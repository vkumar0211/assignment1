@isTest

public class TestContactCreation {
    static testMethod void testContactCreationFromAcc() {
        Test.startTest();

        List<Account> accs = new List<Account>();
        for (Integer i = 0; i < 100; i++) {
            Account a = new Account();
            a.Name = 'DeloitteAssignment ' + i;
            accs.add(a);
        }
        insert accs;

        List<Contact> cons = new List<Contact>();
        cons = [
                SELECT Id, LastName, Account.Name
                FROM Contact
                where LastName like 'DeloitteAssignment%'
        ];

        System.assertEquals(100,cons.size());
       /* for (Integer l = 0; l < 200; l++) {
            System.assertEquals(cons[l].LastName, cons[l].Account.Name);
        }*/

    /*    accs = [
                SELECT Id, Name, (select Id, LastName FROM Contacts)
                FROM Account
                where Name like 'DeloitteAssignment%'
        ];

        for (Account a : accs) {
            Integer conCount = a.Contacts.size();
            System.assertNotEquals(0, conCount);
        }
*/
        Test.stopTest();
    }
}