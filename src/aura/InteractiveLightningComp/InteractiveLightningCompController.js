({
	handleClick : function(component, event, helper) {
		component.set('v.isOpen',true);
	},
	closeModal : function(component,event,helper){
	    component.set('v.isOpen',false);
   },
	cancelModal :function (component,event,helper){
	 helper.showMessage(component,event,helper);
    },
   confirmModal: function(component, event, helper){
     helper.showMessage(component,event,helper);
   }
})